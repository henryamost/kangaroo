## 袋鼠数据库工具 [中文官网](https://www.datatable.online/zh/) [英文官网](https://www.datatable.online/)
为热门数据库系统打造的 SQL 客户端和管理工具(SQLite / MySQL / PostgreSQL / ...) ，支持 Windows / Linux / MacOS


## 支持的数据库系统
数据库支持能力级别: __已计划__ / __部分__ / __支持(:100:)__

| 数据库       | 支持版本 | SQL 查询     | 数据编辑   | 表设计器  | 导出    | 导入    | 智能提示      | 模型化 | 数据同步 |
|-------------|---------|--------------|------------|----------|---------|--------|---------------|-------|---------|
| SQLite      | 3.0 +   | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| MySQL       | 5.5 +   | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| MariaDB     | 10.0 +  | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| PostgreSQL  | 9.0 +   | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| Redis       |         | 已计划   | 已计划   | 已计划   | 已计划   | 已计划   | 已计划   | 已计划  | 已计划 |
| Oracle      |         |           |           |           |           |           |           |          |         |
| SQL Server  |         |           |           |           |           |           |           |          |         |

## 版本发布
开发版本按周发布，计划一周或两周发布一个, 稳定版本和长周期支持版本(LTS) 根据版本质量评估情况不定期发布。

| 版本类型    | Windows(64 bit)   | Linux(64 bit)   | MacOS(64 bit)   |
|-------------|-------------------|-------------------|-----------------|
| 稳定版本 | [Kangaroo 2020(Beta 6)](https://www.datatable.online/zh/download/v1.0.6.201109) | [Kangaroo 2020(Beta 6)](https://www.datatable.online/zh/download/v1.0.6.201109) | [Kangaroo 2020(Beta 6)](https://www.datatable.online/zh/download/v1.0.6.201109) |
| 开发版本 | [Kangaroo 2021(Preview 1)](https://www.datatable.online/zh/download/v1.3.1.201102) | [Kangaroo 2021(Preview 1)](https://www.datatable.online/zh/download/v1.3.1.201102) | [Kangaroo 2021(Preview 1)](https://www.datatable.online/zh/download/v1.3.1.201102) |

## 支持和赞助项目
如果您觉得袋鼠数据库工具有用且愿意支持它持续丰富功能，您可以通过如下方式支持和赞助项目（点击链接或扫码即可通过 PayPal / 微信支持 / 支付宝付款）.<br/>
![支持项目](./images/pay_wide.png) [![./images/pay_wide.png](./images/buymeacoffee.png)](https://www.buymeacoffee.com/dbkangaroo) 

## 更多技术支持

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><img src="https://gitee.com/dbkangaroo/kangaroo/raw/master/images/kangaroo_wx.png" width="200" height="200"  alt="WeChat"/></td>
    <td align="center"><img src="https://gitee.com/dbkangaroo/kangaroo/raw/master/images/kangaroo_qq.png" width="200" height="200"  alt="QQ"/></td>
    <td align="center" style="width:200px;">&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td align="center">微信交流群</td>
    <td align="center">QQ 交流群</td>
    <td align="center" style="width:200px;">微信公众号</td>
  </tr>
</table>

## 工具界面快照
![链接首页](./images/kangaroo-start.png)
![链接登录后页面](./images/kangaroo-tools.png)
![数据网格（自定义列）](./images/kangaroo-grid.png)
![数据网格（条件查询）](./images/kangaroo-grid2.png)
![数据编辑表单](./images/kangaroo-form.png)
![查询界面](./images/kangaroo-query.png)
![表设计器](./images/kangaroo-designer.png)
![表设计器代码预览](./images/kangaroo-designer2.png)
![数据导出界面](./images/kangaroo-export.png)
![数据导入界面](./images/kangaroo-import.png)
![工具配置界面](./images/kangaroo-setting.png)
